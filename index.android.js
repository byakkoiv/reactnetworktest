import React, { Component } from 'react';
import { AppRegistry, ListView, Text, View } from 'react-native';

export default class NetworkTest extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    this.state = {
      dataSource: ds.cloneWithRows([])
    };
  }
  
  componentWillMount() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    this._fetchData().then((response) => {
      this.setState({
        dataSource: ds.cloneWithRows(response)
      })
    })
  }

  _fetchData() {
    return fetch('http://192.168.1.77:3000/products.json').then((response) => response.json());
  }

  render() {
    console.log(this.state.dataSource);
    return (
        <View style={{ flex: 1, paddingTop: 22 }}>
          <ListView
            style={{margin: 20}}
            enableEmptySections={true}
            dataSource={this.state.dataSource}
            renderRow={ (rowData) => <Text>{ rowData.title } ${ rowData.price }</Text>}
          />
        </View>
    );
  }
}
AppRegistry.registerComponent('NetworkTest', () => NetworkTest);
